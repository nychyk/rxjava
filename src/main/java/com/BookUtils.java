package com;

import rx.Observable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yriy on 09/14/16.
 */
public class BookUtils {

    static List<Book> books;

    public static Observable<Book> getData(){

        loadBooks();
        System.out.println("*** Getting books ***");
        return
                Observable.create(subscriber -> {
                    for (int i = 0; i < books.size(); i++) {
                        subscriber.onNext(books.get(i));
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            subscriber.onError(new Throwable("Error in getting book info"));
                        }
                    }
                    subscriber.onCompleted();
                });
    }

    public static List<Book> loadBooks(){
        if (books == null) {
            books = new ArrayList<>();
            books.add(new Book("A Game of Thrones. 5 book", "George R. R. Martin", 694, 350.));
            books.add(new Book("Long Walk To Freedom", "Nelson Mandela", 512, 49.5));
            books.add(new Book("Spring in Action", "Craig Walls", 725, 60.8));
        }
        return books;
    }

}
