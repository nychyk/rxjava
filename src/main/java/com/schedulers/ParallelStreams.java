package com.schedulers;

import com.Book;
import com.BookUtils;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.List;

/**
 * Created by Yriy on 09/14/16.
 */
public class ParallelStreams {

    public static void main(String[] args) {
        List<Book> books = BookUtils.loadBooks();

        Observable<Book> observableBooks = Observable.from(books);

        observableBooks
                .flatMap(book -> Observable.just(book)
                        .subscribeOn(Schedulers.computation())  // new thread for each observable
                        .map(b -> someActionOnNextBook(b))
                )
                .subscribe(book -> System.out.println("Subscriber got " + book.getName() + " on  " +
                        Thread.currentThread().getName())
                );

        // Just to keep the program running
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Book someActionOnNextBook(Book book) {
        try {
            System.out.println("** Some action with '" + book.getName() + " on " + Thread.currentThread().getName());

            Thread.sleep((int) (Math.random() * 500));
            return book;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
