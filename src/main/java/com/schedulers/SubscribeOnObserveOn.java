package com.schedulers;

import com.Book;
import com.BookUtils;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.List;

/**
 * Created by Yriy on 09/14/16.
 */
public class SubscribeOnObserveOn {

    public static void main(String[] args) {
        List<Book> books = BookUtils.loadBooks();
        Observable<Book> observableBooks = null;

        observableBooks.from(books)
                .subscribeOn(Schedulers.computation())  // push data on computation thread
                .doOnNext(book -> log(book))            // Side effect: Log on computation thread
                .observeOn(Schedulers.io())             // Process on another io thread
                .subscribe(book -> someActionOnNext(book));

        // Sleep just to keep the program running
        try {
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void someActionOnNext(Book book){
        try {
            System.out.println("** Some Action with '" + book.getName() +
                    "' on " + Thread.currentThread().getName());
            Thread.sleep((int)(Math.random()*500));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void log(Book book){
        System.out.println("===> Logging '" + book.getName() +
                "' on "  + Thread.currentThread().getName() );
    }
}
