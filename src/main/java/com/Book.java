package com;

/**
 * Created by yurii.nychyk on 9/14/2016.
 */
public class Book {
    private String name;
    private String author;
    private Integer numberOfPages;
    private Double price;

    public Book(String name, String author, Integer numberOfPages, Double price) {
        this.name = name;
        this.author = author;
        this.numberOfPages = numberOfPages;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", numberOfPages=" + numberOfPages +
                ", price=$" + price +
                '}';
    }
}
