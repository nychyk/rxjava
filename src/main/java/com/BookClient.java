package com;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Yriy on 09/14/16.
 */
public class BookClient {
    public static void main(String[] args) {
        Observable<Book> bookData = BookUtils.getData(); // no data coming in yet!!!

        bookData.subscribe(new Subscriber<Book>() {
            public void onNext(Book book) {
                System.out.println(book);
            }

            public void onError(Throwable throwable) {
                System.err.println("Client received: " + throwable.getMessage());
            }

            public void onCompleted() {
                System.out.println("*** The stream is over ***");
            }
        });
    }
}
