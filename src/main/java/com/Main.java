package com;

import rx.Observable;

import java.util.List;

/**
 * Created by yurii.nychyk on 9/14/2016.
 */
public class Main {
    public static void main(String[] args) {
        List<Book> books = BookUtils.loadBooks();

        //Java Iterable: pull
        books.forEach(book -> {
            if ("Craig Walls".equals(book.getAuthor())) {
                System.out.println(book.getName() + ", " + book.getNumberOfPages() + " pages");
            }
        });

        //Java 8 Stream: a pull
        books.stream()
                .skip(1)
                .filter(book -> "Craig Walls".equals(book.getAuthor()))
                .map(book -> book.getName() + ": $" + book.getPrice())
                .forEach(book -> System.out.println(book));

        Observable<Book> observableBook = Observable.from(books);

        //Rx Observable: push
        observableBook
                .skip(1)
                .take(1)
                .distinct(book -> book.getName())
                .map(b -> b.getName() + ": $" + b.getPrice())
                .subscribe(
                        book -> System.out.println(book),
                        err -> System.out.println(err),
                        () -> System.out.println("Streaming is complete")
                );
    }
}
